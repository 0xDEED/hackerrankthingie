﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AngleSharp;

namespace HackerRankChallenge
{
    internal class Program
    {
        static async Task<Dictionary<char, char>> CreateDecryptionDictonary()
        {
            var config = Configuration.Default.WithDefaultLoader();
            const string dicBuilderStr = "abcdefghijklmnopqrstuvwxyz 0123456789 ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            var address = $"http://protext.hackerrank.com/dw?text={WebUtility.UrlEncode(dicBuilderStr)}"; //We can browse to any url, helpfully the site returns any string in it's encrypted format.

            var document = await BrowsingContext.New(config).OpenAsync(address);
            const string cellSelector = "#obfuscated";
            var cells = document.QuerySelectorAll(cellSelector);
            var obfuscatedStr = cells.Select(m => m.TextContent).ToList()[0];
            var decryptionDictionary = new Dictionary<char, char>();

            for (var i = 0; i < obfuscatedStr.Length-1; i++)
            {

                if (!decryptionDictionary.ContainsKey(obfuscatedStr[i]))
                {
                    decryptionDictionary.Add(obfuscatedStr[i], dicBuilderStr[i]);
                }
            }
            return decryptionDictionary;
        }

        static async Task<string> GetChallenge()
        {
            var config = Configuration.Default.WithDefaultLoader();
            const string address = "http://protext.hackerrank.com/dw";
            var document = await BrowsingContext.New(config).OpenAsync(address);
            const string cellSelector = "#gibberish";
            var cells = document.QuerySelectorAll(cellSelector);
            var obfuscatedStr = cells.Select(m => m.TextContent).ToList()[0];
            return obfuscatedStr;
        }
        private static void Main()
        {
            var decrypDic = CreateDecryptionDictonary().Result;
            var obfuscatedStr = GetChallenge().Result;
            var deobf ="";

            foreach (var character in obfuscatedStr)
            {
                char outval;

                if (!decrypDic.TryGetValue(character, out outval))
                {
                    Console.WriteLine("Dictonary Incomplete");
                    break;
                }
                deobf += outval;
            }
            Console.WriteLine("Deobfuscated String = "+deobf);
            Console.ReadKey();
        }
    }
}
